def enc_one(c): # takes 1 char, return a list of bytes (1 ~ 4)
                # byte = a string of 0 or 1 of len 8
                #        e.g. '01010110'
    codepoint = ord(c)
    binary_codepoint = bin(codepoint)[2:]
    n = len(binary_codepoint)
    if n <= 7:
        return ['0' * (8 - n) + binary_codepoint]
    elif n <= 11:
        padded = '0' * (11 - n) + binary_codepoint
        return ['110' + padded[:5], '10' + padded[5:]]
    elif n <= 16:
        padded = '0' * (16 - n) + binary_codepoint
        return ['1110' + padded[:4],
                '10' + padded[4:10],
                '10' + padded[10:]]
    else:
        # n <= 21
        padded = '0' * (21 - n) + binary_codepoint
        return ['11110' + padded[:3],
                '10' + padded[3:9],
                '10' + padded[9:15],
                '10' + padded[15:]]

