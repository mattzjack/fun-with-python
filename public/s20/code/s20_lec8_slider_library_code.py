import random, copy

def swap(m, r0, c0, r1, c1):
  m[r0][c0], m[r1][c1] = m[r1][c1], m[r0][c0]

class Slider(object):
  def __init__(self, m=3, n=3):
    self.m = m
    self.n = n
    self.dimension = m * n
    self.matrix = [[None if r == m - 1 and c == n - 1
                         else r * n + c for c in range(n)] for r in range(m)]
    self.pos = self.get_pos()
    self.history = []

  def __repr__(self):
    return '\n'.join(map(lambda row: ', '.join(map(lambda x: '*' if x == None
                                                                 else str(x),
                                                   row)), self.matrix))

  def __eq__(self, other):
    return isinstance(other, Slider) and self.matrix == other.matrix

  def __hash__(self):
    return hash(tuple(map(tuple, self.matrix)))

  def __lt__(self, other):
    return True

  def get_pos(self):
    d = dict()
    for r in range(self.m):
      for c in range(self.n):
        d[self.matrix[r][c]] = (r, c)
    return d

  def find(self, x):
    return self.pos[x]

  def get(self, r, c):
    return self.matrix[r][c]

  def nrows(self):
    return self.m

  def ncols(self):
    return self.n

  def dim(self):
    return self.dimension

  def is_legal_move(self, d):
    r, c = self.pos[None]
    return ((d == 'up' and r > 0) or
            (d == 'down' and r < self.m - 1) or
            (d == 'left' and c > 0) or
            (d == 'right' and c < self.n - 1))

  def move(self, d):
    r, c = self.pos[None]
    self.history.append(d)
    if d == 'up' and r > 0:
      above = self.matrix[r - 1][c]
      swap(self.matrix, r, c, r - 1, c)
      self.pos[None] = (r - 1, c)
      self.pos[above] = (r, c)
    elif d == 'down' and r < self.m - 1:
      below = self.matrix[r + 1][c]
      swap(self.matrix, r, c, r + 1, c)
      self.pos[None] = (r + 1, c)
      self.pos[below] = (r, c)
    elif d == 'left' and c > 0:
      left = self.matrix[r][c - 1]
      swap(self.matrix, r, c, r, c - 1)
      self.pos[None] = (r, c - 1)
      self.pos[left] = (r, c)
    elif d == 'right' and c < self.n - 1:
      right = self.matrix[r][c + 1]
      swap(self.matrix, r, c, r, c + 1)
      self.pos[None] = (r, c + 1)
      self.pos[right] = (r, c)
    else:
      self.history.pop()
      raise Exception('did not move: illegal move')

  def is_solved(self):
    for k in self.pos:
      if ((k == None and self.pos[k] != (self.m - 1, self.n - 1)) or
          (k != None and self.pos[k] != (k // self.n, k % self.n))):
        return False
    return True

  def shuffle(self, n=None):
    if n == None:
      n = self.dimension * 100
    for _ in range(n):
      try:
        self.move(random.choice(['up', 'down', 'left', 'right']))
      except:
        pass

  def my_copy(self):
    return copy.deepcopy(self)

