def start_end(f):
    def g(*args, **kwargs):
        print(f'[{f.__name__}] started')
        f(*args, **kwargs)
        print(f'[{f.__name__}] ended')
    return g

