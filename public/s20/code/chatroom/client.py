from socket import socket
from socket_utils import *
from threading import Thread

@start_end
def send(server, kill):
    while not kill[0]:
        cmd = input('> ')
        if cmd.startswith('/'):
            if cmd[1:] == 'exit':
                kill[0] = True
            else:
                print('invalid!')
        else:
            server.send(cmd.encode())

@start_end
def main():
    HOST = '128.2.13.144'
    PORT = 50001
    server = socket()
    print('connecting to server...')
    server.connect((HOST, PORT))
    print('success! enter some text, then press return to send it to server.')
    print('or, you can enter "/exit" to exit the client.')
    print('remember to shut down server afterwards, by entering "exit" there (no /)')
    server.setblocking(True)
    kill = [False]
    Thread(target=send, args=(server, kill)).start()

main()

