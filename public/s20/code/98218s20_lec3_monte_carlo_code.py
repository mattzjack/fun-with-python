import random

def bday_prob(m, n=1000):
    # m people in a room
    # what are the odds that >= 2 have 
    # the same bday?
    result = 0
    for _ in range(n):
        print('i am outside:', _)
        s = set()
        for _ in range(m):
            print('i am inside:', _)
            day = random.randrange(366)
            s.add(day)
        if len(s) < m:
            result += 1
    return result / n

def bd2(n=1000):
    result = 0
    for _ in range(n): 
        s = set()
        while True:
            day = random.randrange(366)
            if day not in s:
                s.add(day)
            else:
                result += len(s)
                break
    return result / n
    











