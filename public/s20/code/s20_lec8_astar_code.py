import copy

# h: heuristics function

def get_best(states_to_check, h):
    best_state = None
    best_score = None
    for state in states_to_check:
        this_state_score = h(state)
        if best_score == None or this_state_score > best_score:
            best_state = state
            best_score = this_state_score
    return best_state

def get_best2(states_to_check, h):
    best_state = None
    best_score = None
    for state in states_to_check:
        this_state_score = h(state)
        if best_score == None or this_state_score < best_score:
            best_state = state
            best_score = this_state_score
    return best_state

# h1 : higher = better 
# state 1 -> 100
# state 2 -> 0 

# h2 : lower = better
# state 1 -> 10
# state 2 -> 20

def astar_solve(initial_state, h, get_best_fn):
    visited = set()
    if is_solved(initial_state): return initial_state
    states_to_check = [initial_state] # not solved
    nstates = 0
    while len(states_to_check) > 0:
        # print('states to check:')
        # print(states_to_check)
        nstates += 1
        best_state = get_best_fn(states_to_check, h)
        visited.add(best_state)
        states_to_check.remove(best_state)
        children = get_children(best_state) # list of states
        for child in children:
            if child not in visited:
                if not is_solved(child):
                    states_to_check.append(child)
                else:
                    print('states check:', nstates)
                    return child
    print('states check:', nstates)
    return None


def bad_h(state):
    return 0

def slightly_better_h(state):
    # result = sum over 0 - 7 (diff row + diff col between curr pos and target pos)
    result = 0
    for target_r in range(3):
        for target_c in range(3):
            i = target_r * 3 + target_c
            if i == 8: continue
            (curr_r, curr_c) = state.find(i)
            result += abs(target_r - curr_r) + abs(target_c - curr_c)
    return result

# high number = better ===> check
# low number = better  ===> cross

def is_solved(state):
    return state.is_solved()

def get_children(state):
    moves = ['up', 'down', 'left', 'right']
    results = []
    for move in moves:
        if state.is_legal_move(move):
            state2 = state.my_copy()
            state2.move(move)
            results.append(state2)
    # print('got children:')
    # print(results)
    return results