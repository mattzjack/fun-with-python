import string

def caesar_shift(s, key):
    letters = string.ascii_lowercase
    s = s.lower()
    result = ''
    for c in s:
        new_ind = letters.index(c) + key
        shifted = letters[new_ind % 26]
        result += shifted
    return result

# input_file = '/Users/home/Desktop/98218s20/encr_me.txt'
# 
# with open(input_file) as f:
#     s = f.read().strip()
# 
# result = caesar_shift(s, 4)
# print(result)
# 
# with open('/Users/home/Desktop/98218s20/result.txt', 'x') as f: # must be a new file
#     f.write(result)


def otp(pt, key):
    result = ''
    for i in range(len(pt)):
        b = str((int(pt[i]) + int(key[i])) % 2)
        result += b
    return result

pt = '10101000100101011110101010101'
r  = '11111110001111101011111111111'






# actual_r = otp(pt, k)
# assert(actual_r == r)
# print('yay')
# attempt_decr = otp(r, k)
# assert(attempt_decr == pt)
# print('yey')



# k  = '01010110101010110101010101010'

















