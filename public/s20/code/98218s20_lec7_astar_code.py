# h: heuristics function

def get_best(states_to_check, h):
    best_state = None
    best_score = None
    for state in states_to_check:
        this_state_score = h(state)
        if best_score == None or this_state_score > best_score:
            best_state = state
            best_score = this_state_score
    return best_state

def astar_solve(initial_state, h):
    visited = set()
    if is_solved(initial_state): return initial_state
    states_to_check = [initial_state] # not solved
    while len(states_to_check) > 0:
        best_state = get_best(states_to_check, h)
        visited.add(best_state)
        states_to_check.remove(best_state)
        children = get_children(best_state) # list of states
        for child in children:
            if child not in visited:
                if not is_solved(child):
                    states_to_check.append(child)
                else:
                    return child
    return None

def bad_h(state):
    return 0

def is_solved(state):
    return state.is_solved()

def get_children(state):
    moves = ['up', 'down', 'left', 'right']
    results = []
    for move in moves:
        if state.is_legal_move(move):
            state2 = copy.copy(state)
            state2.move(move)
            results.append(state2)
    return results