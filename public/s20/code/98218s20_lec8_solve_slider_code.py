from s20_lec8_slider_library_code import *
from s20_lec8_astar_code import *
import time

# s = Slider()
# s.shuffle(n=100)
# print('initial:')
# print(s)
# input('press enter to start solving')
# result = astar_solve(s, bad_h)
# print('result:')
# print(result)

def test_solve_slider_bad_h():
    for n in [160]:
        avg_t = 0
        for _ in range(20):
            s = Slider()
            s.shuffle(n=n)
            t0 = time.time()
            result = astar_solve(s, slightly_better_h, get_best2)
            t1 = time.time()
            assert(result != None)
            avg_t += t1-t0
        avg_t /= 20
        print(f'n={n}, t={avg_t}')

test_solve_slider_bad_h()
