from socket import socket
from threading import Thread
import time

# next time:
## make it work
## make server relay the msgs to everyone instead just receiving

def handle_client(server, clients, client_connection, client_id, kill):
    print(f'handling client {client_id}')
    while not kill[0]:
        some_bytes = client_connection.recv(16)
        msg = some_bytes.decode()
        print(f'{client_id}: {msg}')

def listen(server, clients, kill):
    print('listening for connections!')
    next_id = 0
    while not kill[0]:
        new_connection, address = server.accept()
        new_id = next_id
        next_id += 1
        print(f'new connection: id {new_id}')
        new_connection.setblocking(True)
        clients.append(new_connection)
        Thread(target=handle_client, args=(server, clients, new_connection, new_id, kill)).start()

def main():
    print('server started!')
    HOST = '' # usually: ip address
    PORT = 57642 # random
    server = socket()
    server.bind((HOST, PORT))
    clients = []
    kill = [False]
    Thread(target=listen, args=(server, clients, kill)).start()
    print('enter "exit" to exit')
    while not kill[0]:
        cmd = input()
        if cmd == 'exit':
            kill[0] = True
        else:
            print('invalid!')
    

main()
