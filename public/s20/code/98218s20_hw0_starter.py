# comment these out if you don't want them tested

def main():
    test_dec_one()
    test_enc()
    test_dec()

# write your implementations here

def dec_one(l):
    return 42

def enc(s):
    return 42

def dec(l):
    return 42

##### code for testing #####

import random, functools

# from lec 0
def enc_one(c): # takes 1 char, return a list of bytes (1 ~ 4)
                # byte = a string of 0 or 1 of len 8
                #        e.g. '01010110'
    codepoint = ord(c)
    binary_codepoint = bin(codepoint)[2:]
    n = len(binary_codepoint)
    if n <= 7:
        return ['0' * (8 - n) + binary_codepoint]
    elif n <= 11:
        padded = '0' * (11 - n) + binary_codepoint
        return ['110' + padded[:5], '10' + padded[5:]]
    elif n <= 16:
        padded = '0' * (16 - n) + binary_codepoint
        return ['1110' + padded[:4],
                '10' + padded[4:10],
                '10' + padded[10:]]
    else:
        # n <= 21
        padded = '0' * (21 - n) + binary_codepoint
        return ['11110' + padded[:3],
                '10' + padded[3:9],
                '10' + padded[9:15],
                '10' + padded[15:]]

def test_dec_one():
    print('testing dec_one', end='...')
    for _ in range(100):
        c = chr(random.randrange(0xcfff))
        result = dec_one(enc_one(c))
        if result != c:
            print(f'failed!\ninput: {enc_one(c)!r}\nexpect: {c!r}\ngot: {result!r}')
            print()
            return
    print('passed!')
    print()

def test_enc():
    print('testing enc', end='...')
    refsoln = lambda a:functools.reduce(lambda b,c:b+c,map(enc_one,a))
    for _ in range(100):
        n = random.randrange(10, 50)
        s = ''.join([chr(random.randrange(0xcfff)) for _ in range(n)])
        expect = refsoln(s)
        get = enc(s)
        if expect != get:
            print(f'failed!\ninput: {s!r}\nexpect: {expect!r}\ngot: {get!r}')
            print()
            return
    print('passed!')
    print()

def test_dec():
    print('testing dec', end='...')
    refenc = lambda a:functools.reduce(lambda b,c:b+c,map(enc_one,a))
    for _ in range(100):
        n = random.randrange(10, 50)
        s = ''.join([chr(random.randrange(0xcfff)) for _ in range(n)])
        result = dec(refenc(s))
        if result != s:
            print(f'failed!\ninput: {refenc(s)!r}\nexpect: {s!r}\ngot: {result!r}')
            print()
            return
    print('passed!')
    print()

main()

