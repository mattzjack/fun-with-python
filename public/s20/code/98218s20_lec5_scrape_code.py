# in terminal / powershell

# mac: (probably)

# python3 -m pip install requests
# python3 -m pip install beautifulsoup4

# pc: (probably)

# py -m pip install requests
# py -m pip install beautifulsoup4

# in python:

import requests
from bs4 import BeautifulSoup

query_text = input('which wikipedia page? ')

r = requests.get(f'https://en.wikipedia.org/wiki/{query_text}')
soup = BeautifulSoup(r.text, 'html.parser')

print(soup.find(id='mw-content-text').div.find_all('p')[2].text)


