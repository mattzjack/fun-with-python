from PIL import Image
# from tqdm import trange
# -m pip install tqdm

# https://pillow.readthedocs.io/


# im = Image.open(path)
# 
# im.show()
# 
# im.size # =>  (1000, 500)
# im.mode # => 'RGB' 'RGBA'
# im.tobytes() # => b'\x00\xff...'
# im.frombytes() # inverse of tobytes
# 
# b'\x00\xff\xae\x92\x6a'.hex() # '00ffae926a'
# bytes.fromhex('6b') # b'6b'
# bytes([3, 255]) # => b'\x03\xff'

from_path = './small.png'
msg = '1000101010101010110010101010111010'

def put_in(n, b):
    if b == '1' and n % 2 == 0:
        n += 1
    elif b == '0' and n % 2 == 1:
        n -= 1
    return bytes([n])

def steg(from_path, msg): # return a im object
    im = Image.open(from_path)
    (w, h) = im.size
    total_capacity = w * h * 3
    assert(len(msg) <= total_capacity - 1)
    msg = '0' * (total_capacity - 1 - len(msg)) + '1' + msg
    assert(len(msg) == total_capacity)
    assert(im.mode == 'RGBA')
    bs = im.tobytes()
    result = b''
    # build pixels into result, only RGB
    msg_pt = 0
    # for i in trange(0, len(bs), 4):
    for i in range(0, len(bs), 4):
        r = bs[i]
        bit = msg[msg_pt]
        msg_pt += 1
        newr = put_in(r, bit)
        result += newr

        g = bs[i+1]
        bit = msg[msg_pt]
        msg_pt += 1
        newg = put_in(g, bit)
        result += newg

        b = bs[i+2]
        bit = msg[msg_pt]
        msg_pt += 1
        newb = put_in(b, bit)
        result += newb
    assert(msg_pt == total_capacity)
    return Image.frombytes('RGB', (w, h), result)

im = steg(from_path, msg)
im.show()










