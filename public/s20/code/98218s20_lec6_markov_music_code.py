import random

states = ['c4', 'd4']

               # to c4    to d4
transitions = [[    .5,       .5], # from c4
               [   .2 ,       .8]] # from d4


start = 0

# goal: generate 100 notes sequence

result = [0]

for _ in range(99):
    last_state = result[-1]
    r = random.random()
    if r < transitions[last_state][0]:
        result.append(0)
    else:
        result.append(1)

notes = []
for x in result:
    notes.append(states[x])

print(notes)

