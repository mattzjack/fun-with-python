import time
from threading import Thread

# modify main to only test what you want

def main():
    print('running f0')
    f0()
    print('running f1')
    f1()

def f0():
    # create a new thread here!
    # Thread(target=..., args=...).start()
    for _ in range(10):
        print(42)
        time.sleep(1)

def f0_thread():
    # you probably want this
    print('something here')

def f1():
    L = []
    # create a new thread here!
    # Thread(target=..., args=...).start()
    for _ in range(5):
        if len(L) == 0:
            print('still empty...')
            time.sleep(1)
        else:
            print('nonempty! success!')
            return
    print('too slow. failed')

def f1_thread():
    print('this should probably take some parameter')

main()


