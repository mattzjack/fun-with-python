import os

def make_bytes(bstr):
  assert(isinstance(bstr, str) and                        # it's a string
         bstr.replace('0', '').replace('1', '') == '' and # it only has 0 and 1
         len(bstr) % 8 == 0)                              # length is divisible by 8
  res = b''
  for i in range(0, len(bstr), 8):
    this_byte = bstr[i:i+8]
    n = int(this_byte, 2)
    this_hex = '{:02x}'.format(n)
    res += bytes.fromhex(this_hex)
  return res

def write_bytes_to_new_file(bytes_obj, filename='newfile.txt'):
  assert(filename not in os.listdir()) # has to be a new file
  with open(filename, "xb") as f:
    f.write(bytes_obj)

def write_bstr_to_new_file(bstr, filename='newfile.txt'):
  assert(filename not in os.listdir()) # has to be a new file
  with open(filename, "x") as f:
    f.write(bstr)

def enc(text):
  res = ''
  for c in text:
    binary = bin(ord(c))[2:]
    res += '0' * (8-len(binary)) + binary
  return res

def dec(code):
  s = ''
  for i in range(0, len(code), 8):
    this8bits = code[i:i+8]
    asADecNum = int(this8bits, 2)
    thisChar = chr(asADecNum)
    s += thisChar
  return s
