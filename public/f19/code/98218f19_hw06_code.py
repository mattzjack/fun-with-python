import requests, bs4, urllib.request, urllib.parse

# downloads file from url to path/filename. must be a new filename
def download(url, path):
    content = urllib.request.urlopen(url).read()
    with open(path, 'xb') as f:
        f.write(content)


