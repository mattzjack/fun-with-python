from PIL import Image

path = '/Users/zosimus/Pictures/Darchrow.JPG'

def steg(img_path, msg):
    im = Image.open(path)
    b0 = im.tobytes()
    (w, h) = im.size
    # scheme: 00000..0001{msg}
    max_allowed_msg_len = 3 * w * h - 1
    assert(len(msg) <= max_allowed_msg_len)
    msg = '1' + msg
    msg = '0' * (3 * w * h - len(msg)) + msg
    assert(len(msg) == len(b0))
    b1 = b''
    for i in b0:
        # i is an int
        if msg[i] == '0' and i % 2 == 0:
            pass
        elif msg[i] == '1' and i % 2 == 1:
            pass
        elif msg[i] == '0' and i % 2 == 1:
            i -= 1
        else:
            # msg[i] == '1' and i % 2 == 0:
            i += 1
        b1 += bytes([i])
    im1 = Image.frombytes('RGB', im.size, b1)
    return im1

def unsteg(img_path):
    im = Image.open(img_path)
    b0 = im.tobytes()
    msg = ''
    for i in b0:
        msg += str(i % 2)
    one_index = msg.index('1')
    return msg[one_index + 1 :]




