import requests, bs4, urllib.request

webpage_url = 'https://www.andrew.cmu.edu/user/ymkong/98218/f19/'

r = requests.get(webpage_url)
soup = bs4.BeautifulSoup(r.text, 'html.parser')
next_hw = soup.table.find_all('tr')[7].find_all('td')[2].string.strip()
if next_hw != '[*]':
    print('do hw!')
else:
    print('no hw yet!')

# download all images

# imgs = soup.find_all('img')
# imgurls = [im['src'] for im in imgs]
# 
# save_folder = '/Users/zosimus/Documents/cmu/19h/98218/6/'
# 
# for i in range(len(imgurls)):
#     this_url = imgurls[i]
#     if this_url.startswith('//'):
#         urllib.request.urlretrieve('https:' + this_url,
#                                    # save_folder + str(i) + '.png')
#                                    f'{save_folder}{i}.png')

# change all carbon into nitrogen

# s = r.text.replace('carbon', 'nitrogen')

# write s to my computer

