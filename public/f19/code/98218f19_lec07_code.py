from slider import *
import copy

def dumb_h(s):
    return 0

def better_h(s):
    res = 0
    for i in range(0, s.nrows() * s.ncols() - 1):
        (r, c) = s.find(i)
        r0 = i // s.ncols()
        c0 = i % s.ncols()
        res += abs(r0 - r) + abs(c0 - c)
    return res

def best_state(todo_list, h):
    min_score = None
    min_state = None
    for s in todo_list:
        score = h(s)
        if min_score == None or score < min_score:
            min_score = score
            min_state = s
    return min_state

def get_neighbor_states(s):
    res = set()
    for move in ['up', 'down', 'left', 'right']:
        if s.is_legal_move(move):
            s2 = s.my_copy()
            s2.move(move)
            res.add((s2, move))
    return res

def solve(s, h): # h is the heuristics fn
    if s.is_solved():
        return s.history
    # either return 'no soln' or [moves]
    visited = set()
    # everything in the todo_list is pre-checked to be nonsolved
    # which means: check if solved before putting into list
    todo_list = {s}
    while len(todo_list) > 0:
        # get best state out of todo
        s_best = best_state(todo_list, h)
        # print('s_best')
        # print(s_best)
        visited.add(s_best)
        # print('visited')
        # for tmp_s in visited:
        #     print(tmp_s)
        todo_list.remove(s_best)
        # print('todo')
        # for tmp_s in todo_list:
        #     print(tmp_s)
        neighbor_states = get_neighbor_states(s_best) # set
        for (s2, move) in neighbor_states:
            # print('neighbor')
            # print(s2)
            if s2 not in visited:
                if s2.is_solved():
                    print('success!')
                    print(len(visited))
                    return s2.history
                todo_list.add(s2)
    print('impossible')


def test(n, h):
    s = Slider(n, n)
    s.shuffle(50)
    print('init:')
    print(s)
    res = solve(s, h)
    print('solved:')
    print(s)
    return res

