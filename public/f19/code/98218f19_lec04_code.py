import random
from tqdm import *

def bd(n, npeople):
    success = 0
    fail = 0
    for _ in range(n):
        l = [random.randrange(365) for _ in range(npeople)]
        s = set(l)
        if len(s) < npeople:
            success += 1
        else:
            fail += 1
    return success / n

def bd2(m):
    total = 0
    for _ in trange(m):
        s = set()
        while True:
            n = random.randrange(365)
            if n in s:
                total += len(s)
                break
            else:
                s.add(n)
    return total / m

def bd3(p, n):
    lo = 2
    hi = 366
    while hi - lo > 1:
        mid = (hi + lo) // 2
        prob = bd(n, mid)
        if prob > p:
            hi = mid
        else:
            lo = mid
    return lo


def coupon(n, m):
    total = 0
    for _ in trange(m):
        time = 0
        have = set()
        while True:
            time += 1
            have.add(random.randrange(n))
            if len(have) == n:
                total += time
                break
    return total / m






