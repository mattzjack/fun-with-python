import string, random

def shiftOne(c, key):
  # c is one uppercase letter
  # key is a int
  a = string.ascii_uppercase
  newInd = (a.index(c) + key) % 26
  return a[newInd]

def caesar_shift(msg, key):
  result = ''
  for c in msg:
    c1 = shiftOne(c, key)
    result += c1
  return result

def caesar_decr(code, key):
  # the encr key and the decr key should be the same
  return caesar_shift(code, -key)

def bin_add(b0, b1):
  # both are str of len 1
  if b0 == '0':
    return b1
  else:
    if b1 == '1':
      return '0'
    else:
      return '1'

def otp(msg, key):
  # both are 01-str of the same length
  result = ''
  for i in range(len(msg)):
    result += bin_add(msg[i], key[i])
  return result

def decr_otp(msg, key):
  return otp(msg, key)

def kpa_otp(plaintext, code):
  return otp(plaintext, code)


def gen_otp_key(n):
  result = ''
  for i in range(n):
    result += random.choice('01')
  return result
  



def enc(text):
  res = ''
  for c in text:
    binary = bin(ord(c))[2:]
    res += '0' * (8-len(binary)) + binary
  return res

def dec(code):
  s = ''
  for i in range(0, len(code), 8):
    this8bits = code[i:i+8]
    asADecNum = int(this8bits, 2)
    thisChar = chr(asADecNum)
    s += thisChar
  return s

def discrete_exp(b, e, p):
  return (b ** e) % p

def diffie_hellman(p, g, g_b):
  a = random.randrange(p)
  g_a = discrete_exp(g, a, p)
  g_ab = discrete_exp(g_b, a, p)
  return (g_a, g_ab)

