from random import shuffle, randrange

def pancake_flip(l, i):
    return 42

def pancake_sort(l):
    return 42

def test_pancake_flip():
    print('Testing pancake_flip', end='...')
    mpf=lambda a,c:[a[b if b >= c else c-b-1] for b in range(len(a))]
    L = list(range(20))
    for _ in range(20):
        shuffle(L)
        i = randrange(len(L) + 1)
        correct = mpf(L, i)
        pancake_flip(L, i)
        assert(L == correct)
    print('Passed!')

def test_pancake_sort():
    print('Testing pancake_flip', end='...')
    L = list(range(20))
    for _ in range(20):
        shuffle(L)
        pancake_sort(L)
        assert(L == list(range(20)))
    print('Passed!')

test_pancake_flip()
test_pancake_sort()

