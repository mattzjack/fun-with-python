# given a map f : char => ascii number in decimal
# write  a function that returns a string of 0s and 1s that represents the ascii encoding of the input text
def enc(text):
  res = ''
  for c in text:
    binary = bin(ord(c))[2:]
    res += '0' * (8-len(binary)) + binary
  return res

def dec(code):
  s = ''
  for i in range(0, len(code), 8):
    this8bits = code[i:i+8]
    asADecNum = int(this8bits, 2)
    thisChar = chr(asADecNum)
    s += thisChar
  return s

# print(dec(enc("I eat laptop!")))

# suppose we can convert our 01 string into a bytes object
# function name is bstr2bytes

with open("lunchinstruction.txt", "w+b") as f:
  f.write(bstr2bytes(enc("I eat laptop!")))

# https://www.andrew.cmu.edu/user/ymkong/98218/











