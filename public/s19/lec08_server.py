from socket import socket
from queue import Queue
from threading import Thread

# execute this file on server
# modify main if necessary

def handle_msg(clientele, msgq):
  # application specific
  # now: prints received message, sends to everyone except sender
  while True:
    (sender_ID, msg) = msgq.get()
    msgq.task_done()
    print('got msg from {}: {!r}'.format(sender_ID, msg))
    msg2 = encr(msg, 4)
    for i in range(len(clientele)):
      if i != sender_ID:
        clientele[i].send('{}: {}\n'.format(sender_ID, msg2).encode())

def handle_client(clientele, client_ID, msgq, bufsize=16, sep='\n'):
  # receives msg from client and puts in queue
  client_conn = clientele[client_ID]
  msg_stream = ''
  while True:
    msg_stream += client_conn.recv(bufsize).decode()
    nmsgs = msg_stream.count(sep)
    msg_segs = msg_stream.split(sep)
    for i in range(nmsgs):
      decrypted_msg = decr(msg_segs[i], -3)
      msgq.put((client_ID, decrypted_msg))
    if nmsgs == len(msg_segs):
      msg_stream = ''
    else:
      msg_stream = msg_segs[-1]

def main():
  # in general: IP of server (the machine running this file)
  HOST = ''
  # magic num: be consistent with client
  PORT = 50001
  server = socket()
  server.bind((HOST, PORT))
  # 0 optional for python 3.5+
  server.listen(0)
  msgq = Queue()
  # list of client connections
  # length = nconns
  # client with ID x is at index x
  clientele = []
  nconns = 0
  Thread(target=handle_msg, args=(clientele, msgq)).start()
  while True:
    # waits until someone connects
    (new_conn, new_address) = server.accept()
    new_conn.setblocking(True)
    new_ID = nconns
    nconns += 1
    clientele.append(new_conn)
    Thread(target=handle_client, args=(clientele, new_ID, msgq)).start()

main()

