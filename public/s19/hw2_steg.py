import tkinter

# encoding library

def enc_cp(n):
    n0 = 2 ** 7
    n1 = 2 ** 11
    n2 = 2 ** 16
    n3 = 2 ** 21
    if n < n0:
        return '{:08b}'.format(n)
    elif n < n1:
        b = '{:011b}'.format(n)
        return '110{}10{}'.format(b[:5], b[5:])
    elif n < n2:
        b = '{:016b}'.format(n)
        return '1110{}10{}10{}'.format(b[:4], b[4:10], b[10:])
    elif n < n3:
        b = '{:016b}'.format(n)
        return '11110{}10{}10{}10{}'.format(b[:3], b[3:9], b[9:15], b[15:])
    else:
        raise Exception('out of range!!!')

def dec_cp(bs):
    if bs[0] == '0':
        return int(bs, 2)
    elif bs[2] == '0':
        return int(bs[3:8]+bs[10:], 2)
    elif bs[3] == '0':
        return int(bs[4:8]+bs[10:16]+bs[18:], 2)
    else:
        return int(bs[5:8]+bs[10:16]+bs[18:24]+bs[26:], 2)

def enc_txt(plaintext):
    result = ''
    for c in plaintext:
        result += enc_cp(ord(c))
    return result

def dec_txt(code):
    result = ''
    i = 0
    while i < len(code):
        if code[i] == '0':
            result += chr(dec_cp(code[i:i+8]))
            i += 8
        elif code[i+2] == '0':
            result += chr(dec_cp(code[i:i+16]))
            i += 16
        elif code[i+3] == '0':
            result += chr(dec_cp(code[i:i+24]))
            i += 24
        else:
            result += chr(dec_cp(code[i:i+32]))
            i += 32
    return result

# steganography

def steg(image_path, bin_message):
  img = tkinter.PhotoImage(file=image_path)
  dim = img.width() * img.height()
  if len(bin_message) > dim * 3 - 1:
    raise Exception('message too long!!!')
  bin_message = '1' + bin_message
  bin_message = '0' * (3 * dim - len(bin_message)) + bin_message
  assert(len(bin_message) == dim * 3)
  counter = 0
  for x in range(img.width()):
    for y in range(img.height()):
      rgb = img.get(x, y).split() # ===> ["100", "72", "210"]
      r = '{:08b}'.format(int(rgb[0]))  # ===> '00110110'
      r = r[0:7] + bin_message[counter] # ===> '00110111'
      r = int(r, 2)
      counter += 1

      g = '{:08b}'.format(int(rgb[1]))  # ===> '00110110'
      g = g[0:7] + bin_message[counter] # ===> '00110111'
      g = int(g, 2)
      counter += 1

      b = '{:08b}'.format(int(rgb[2]))  # ===> '00110110'
      b = b[0:7] + bin_message[counter] # ===> '00110111'
      b = int(b, 2)
      counter += 1
      
      result = ('#' + '{:02x}'.format(r) +
                      '{:02x}'.format(g) +
                      '{:02x}'.format(b))

      img.put(result, to=(x, y))
  img.write('stegged.gif', format='gif')
      
def unsteg(image_path):
  img = tkinter.PhotoImage(file=image_path)
  result = ''
  for x in range(img.width()):
    for y in range(img.height()):
      rgb = img.get(x, y).split()
      for c in rgb:
        c = int(c)
        c = '{:08b}'.format(c)
        result += c[-1]
  j = result.index('1')
  return result[j+1:]

# initialization

tkinter.Tk()

