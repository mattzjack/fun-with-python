import random
import time
# t0 = time.time()
# perform stuff here
# t1 = time.time()
# t1 - t0

def unique(l):
  return len(set(l)) == len(l)
  #for i in range(len(l)):
  #  if l[i] in l[0:i]+l[i+1:len(l)]:
  #    return False
  #return True

def solve_birthday(n):
  t0 = time.time()
  total_people = 0
  for i in range(n):
    l = list()
    while unique(l):
      bday = random.randrange(1, 366)
      l.append(bday)
    total_people += len(l)
  t1 = time.time()
  print('time taken:', t1 - t0)
  return total_people / n

