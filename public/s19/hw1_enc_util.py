import random

def enc_cp(n):
    n0 = 2 ** 7
    n1 = 2 ** 11
    n2 = 2 ** 16
    n3 = 2 ** 21
    if n < n0:
        return '{:08b}'.format(n)
    elif n < n1:
        b = '{:011b}'.format(n)
        return '110{}10{}'.format(b[:5], b[5:])
    elif n < n2:
        b = '{:016b}'.format(n)
        return '1110{}10{}10{}'.format(b[:4], b[4:10], b[10:])
    elif n < n3:
        b = '{:016b}'.format(n)
        return '11110{}10{}10{}10{}'.format(b[:3], b[3:9], b[9:15], b[15:])
    else:
        raise Exception('out of range!!!')

def dec_cp(bs):
    if bs[0] == '0':
        return int(bs, 2)
    elif bs[2] == '0':
        return int(bs[3:8]+bs[10:], 2)
    elif bs[3] == '0':
        return int(bs[4:8]+bs[10:16]+bs[18:], 2)
    else:
        return int(bs[5:8]+bs[10:16]+bs[18:24]+bs[26:], 2)

def enc_txt(plaintext):
    result = ''
    for c in plaintext:
        result += enc_cp(ord(c))
    return result

def dec_txt(code):
    result = ''
    i = 0
    while i < len(code):
        if code[i] == '0':
            result += chr(dec_cp(code[i:i+8]))
            i += 8
        elif code[i+2] == '0':
            result += chr(dec_cp(code[i:i+16]))
            i += 16
        elif code[i+3] == '0':
            result += chr(dec_cp(code[i:i+24]))
            i += 24
        else:
            result += chr(dec_cp(code[i:i+32]))
            i += 32
    return result

def bin_add_xor(b0, b1):
    if b0 != b1: return '1'
    else: return '0'

def bin_add_flip(b0, b1):
    if b1 == '1':
        if b0 == '1':
            return '0'
        else:
            return '1'
    else:
        return b0

def bin_add_b(b0, b1):
    b0 = int(b0)
    b1 = int(b1)
    result = (b0 + b1) % 2
    return str(result)

def one_time_pad(p, k):
    result = ''
    for i in range(len(p)):
        result += bin_add_xor(p[i], k[i])
    return result

def gen_rand_k(n):
    result = ''
    for i in range(n):
        b = random.choice('01')
        result += b
    return result


















