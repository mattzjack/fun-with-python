# disclaimer: I do not guarantee the correctness of this
#             i.e. passing/failing this test does not prove/disprove the
#                  correctness of your code

def test_encoder(n=256):
    # please name your encoder function 'encoder' for this to work
    print("this test only works if you name your encoder function 'encoder'")
    # n: how many tests you want
    m = input('number of tests (enter an integer larger than 0 or leave blank for default {}): '.format(n))
    if m.isdigit() and m != '0': n = int(m)
    elif m != '': print('failed')
    # this is my reference solution
    d2b=lambda x:str(x)if x<2else d2b(x//2)+str(x%2);extlen=lambda b,n:b if len(b)>=n else'0'+extlen(b,n-1);enc=lambda x:extlen(d2b(x),8)if x<128else'110'+extlen(d2b(x),11)[:5]+'10'+extlen(d2b(x),11)[5:]if x<2048else'1110'+extlen(d2b(x),16)[:4]+'10'+extlen(d2b(x),16)[4:10]+'10'+extlen(d2b(x),16)[10:]if x<65536else'11110'+extlen(d2b(x),21)[:3]+'10'+extlen(d2b(x),21)[3:9]+'10'+extlen(d2b(x),21)[9:15]+'10'+extlen(d2b(x),21)[15:]
    import random
    def nice_bytes(s):
        result = ''
        for i in range(0, len(s), 8):
            result += s[i:i+8] + ' '
        return result[:-1]
    failed = False
    print('number tests: {}'.format(n))
    print('testing...')
    for _ in range(n):
        bs = [7, 11, 16, 21]
        nb = random.randrange(4)
        if nb == 0:
            x = random.randrange(128)
        else:
            x = random.randrange(2**bs[nb-1],2**bs[nb])
        r = enc(x)
        s = encoder(x)
        if len(s) % 8 != 0 or len(s) > 32 or s.replace('0','').replace('1','') != '':
            failed = True
            print('test failed!\nencoding: {}\nrefr solution: {}\nyour solution: {!r}'.format(x, nice_bytes(r), s))
        elif r != s:
            failed = True
            print('test failed!\nencoding: {}\nrefr solution: {}\nyour solution: {}'.format(x, nice_bytes(r), nice_bytes(s)))
    if not failed: print('test passed!')

