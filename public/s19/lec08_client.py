from socket import socket
from queue import Queue
from threading import Thread

# execute this file on client
# modify main if necessary

def handle_msg(msgq):
  # application specific
  # now: prints server msg
  while True:
    ready_msg = msgq.get()
    msgq.task_done()
    print(ready_msg)

def handle_server(server, msgq, bufsize=16, sep='\n'):
  msg_stream = ''
  while True:
    # get a bit of message, might not be one full message
    msg_stream += server.recv(bufsize).decode()
    # parse and put all ready messages to msgq
    nmsgs = msg_stream.count(sep)
    msg_segs = msg_stream.split(sep)
    for i in range(nmsgs):
      msgq.put(decr(msg_segs[i], -4))
    if nmsgs == len(msg_segs):
      msg_stream = ''
    else:
      msg_stream = msg_segs[-1]

def main():
  # in general: change to ip of server
  # leave as '' for demo: only works if server on the same machine
  HOST = ''
  # magic number: use the same one as in server.py
  PORT = 50001
  # message separator [hidden]
  sep = '\n'
  server = socket()
  # connect to server
  server.connect((HOST, PORT))
  # do not timeout
  server.setblocking(True)
  msgq = Queue()
  Thread(target=handle_server, args=(server, msgq)).start()
  Thread(target=handle_msg, args=(msgq,)).start()
  # application-specific
  # now: takes key input and sends to server
  while True:
    new_msg = input()
    encrypted_msg = encr(new_msg, 3) + sep
    server.send(encrypted_msg.encode())

main()

