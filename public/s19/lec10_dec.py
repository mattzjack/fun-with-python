def dec_part(l, i, j, results, ind, counter):
  while l[i].startswith('10'):
    i += 1
  result = []
  while i < j:
    if l[i].startswith('0'):
      result.append(dec1(l[i]))
      i += 1
    elif l[i].startswith('110'):
      result.append(dec1(l[i:i+2]))
      i += 2
    elif l[i].startswith('1110'):
      result.append(dec1(l[i:i+3]))
      i += 3
    else:
      result.append(dec1(l[i:i+4]))
      i += 4
  results[ind] = ''.join(result)
  counter[0] += 1

def dec(l):
  n = 10
  results = [None] * (len(l) // n + 10)
  counter = [0]
  nthreads = 0
  for i in range(0, len(l), n):
    nthreads += 1
    Thread(target=dec_part, args=(l, i, min(i + n, len(l)), results, i // n, counter)).start()
  while counter[0] < nthreads:
    pass
  while results[-1] == None:
    results.pop()
  return ''.join(results)

